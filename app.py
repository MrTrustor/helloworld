import os
from flask import Flask

app = Flask(__name__)

app.config['MESSAGE'] = os.environ['MESSAGE'] if "MESSAGE" in os.environ else "Hello, World!"

@app.route('/')
def hello_world():
    """Returns 'Hello, World!' or a custom string specified in the MESSAGE environment variable."""
    return '<h1>{0}</h1>'.format(app.config['MESSAGE'])

@app.route('/healthz')
def healthz():
    """Returns 'OK' to check that the app is running well."""
    return 'OK'

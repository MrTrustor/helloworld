FROM python:3-slim AS build-env

WORKDIR /usr/src/app
COPY requirements.txt ./
RUN pip install --target=./ --no-cache-dir -r requirements.txt

FROM gcr.io/distroless/python3
COPY --from=build-env /usr/src/app /app
WORKDIR /app
ADD app.py /app/
ENV FLASK_APP=app.py
CMD ["-m", "flask", "run", "--host=0.0.0.0"]
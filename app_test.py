import pytest

from app import app

@pytest.fixture
def client():
    """Creates a non-customised client to test the app."""
    with app.test_client() as client:
        yield client

@pytest.fixture
def client_custom():
    """Creates a client with a custom message to test the app."""
    app.config['MESSAGE'] = 'Hello, Test!'
    with app.test_client() as client_custom:
        yield client_custom

def test_healthcheck(client):
    """Tests the healthcheck."""
    rv = client.get('/healthz')
    assert b'OK' in rv.data

def test_home(client):
    """Tests the non-customised home page."""
    rv = client.get('/')
    assert b'Hello, World!' in rv.data

def test_home_custom(client_custom):
    """Tests the customised home page."""
    rv = client_custom.get('/')
    assert b'Hello, Test!' in rv.data
